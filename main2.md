---
title: Introduction to Dynamic Semantics
author: Patrick D. Elliott
date: 25.06.2018
theme: metropolis 
bibliography: main.bib
biblio-title: Bibliography
biblatexoptions: "bibstyle=biblatex-sp-unified,citestyle=sp-authoryear-comp,language=auto,sorting=nyt,maxcitenames=3,maxbibnames=99,natbib=true,autocite=plain,isbn=false,doi=false,url=false,hyperref=auto,uniquename=false,uniquelist=false"
header-includes:
- |
  ```{=latex}
  \input{extra/fontSetup.tex}
  \input{extra/macros.tex}
  \input{extra/extraPackages.tex}
  \lingset{aboveexskip=0pt,belowexskip=0pt,interpartskip=0pt,belowpreambleskip=0pt}
  ```
...

# DyS

## Overview

- In our previous logical system, DPL, formulae could either:

    - Act as tests on input assignments, returning the same assignments.
    - Shrink the set of assignments, taking a set of assignments, and returning
    a subset.
    
- The latter case represented the contribution of existentially quantified
  sentences.
  
- The notion of a *discourse referent* has no direct correlate.


## PLA via \texttt{DyS}

- PLA simplifies things somewhat by having semantic objects
  which correspond directly to *discourse referents*.
  
- N.b. the version of PLA I'm presenting is simplified relative @dekker1994, and
corresponds more closely to @Charlowc's \texttt{DyS}.

- The basic ideas are the same, but unlike PLA, \texttt{DyS} doesn't make use of
  *assignments*. This makes it a little easier to reason about.

## Stacks

- The current state of the discourse is represented as a *stack*, which is just
  going to be a (potentially empty) sequence of objects.
  
  $$
  s = a...xyz
  $$
  
- The objects in the stack represent the *discourse referents* which have been
  introduced over the course of the discourse so far.

## Stacks ii

- Stacks can be *extended* with additional drefs, simply by adding them to (the
  end of) the stack.
  
$$
s = jb
$$

- We define a primitive binary operation that takes a stack $s$, and an object
  $m$, and pushes $m$ to $s$.

$$
\widehat{sm} = hbm
$$

## Stacks iii

- We're also going to define a primitive unary operation $\tau$, which returns
  the last element to be added to a stack.
  
$$
s' = hbm
$$

$$
s'_τ = m
$$

## Syntax of \texttt{DyS}

- For simplicity, I'll take the syntax of \texttt{DyS} to be identical to the
  syntax of \texttt{FOL} (and hence DPL).
  
- With one exception - we have a new syntactic expression $\textit{pro}$,
  which has the same distribution as individual constants and variables – namely
  it can appear as an argument to predicates, e.g.,
  
- $\metalang{hugs}(\metalang{Annie},\textit{pro})$
  
- Unlike in DPL, sentence meanings in \texttt{DyS} are going to be additionally
  relativised to an assignment $g$. We'll see the relevance of this later.

## Semantics of \texttt{DyS}

- Sentence meanings in \texttt{DyS} are going to express *relations between
  stacks*, rather than relations between assignment functions. 
  
- Here's the interpretation rule for a unary predicate:

- If $π$ is a unary predicate and $α$ is an individual constant, then $\evalM[g]{π(α)} =
  \Set{⟨s,s'⟩| s' = \widehat{s\evalM[g]{α}} \metalang{ and } \evalM[g]{α} ∈ \evalM{π}}$
  
- If $π$ is a unary predicate and $α$ is a variable, then $\evalM[g]{π(α)} =
  \Set{⟨s,s'⟩| s' = s \metalang{ and } \evalM[g]{α} ∈ \evalM{π}}$

## Semantics of \texttt{Dys}

- For binary predicates we now technically need four rules, since individual
  constants update the stack, whereas variables don't quantifiers don't:

- If $π$ is a binary predicate and $α,β$ are individual constants, then $\evalM[g]{π(α,β)} =
  \Set{⟨s,s'⟩| s' = \widehat{s\evalM[g]{β}\evalM[g]{α}} \metalang{ and } ⟨\evalM[g]{α},\evalM[g]{β}⟩ ∈ \evalM[g]{π}}$
  
- If $π$ is a binary predicate and $α$ is an individual constants, and $β$ is a variable, then $\evalM[g]{π(α,β)} =
  \Set{⟨s,s'⟩| s' = \widehat{s\evalM[g]{α}} \metalang{ and } ⟨\evalM[g]{α},\evalM[g]{β}⟩ ∈ \evalM[g]{π}}$
  
- If $π$ is a binary predicate and $α$ is a variable, and $β$ is an individual constant, then $\evalM[g]{π(α,β)} =
  \Set{⟨s,s'⟩| s' = \widehat{s\evalM[g]{β}} \metalang{ and } ⟨\evalM[g]{α},\evalM[g]{β}⟩ ∈ \evalM[g]{π}}$
  
- If $π$ is a binary predicate and $α$ and $β$ are variables, then $\evalM[g]{π(α,β)} =
  \Set{⟨s,s'⟩| s' = s \metalang{ and } ⟨\evalM[g]{α},\evalM[g]{β}⟩ ∈ \evalM[g]{π}}$
  
## Semantics of \textt{Dys}

- The general rule is: *individual constants* trigger a push to the output
  stack, whereas *variables* do not.
  
## Semantics of \texttt{DyS}

- Assume a model with *Jeff*, *Britta*, and *Annie*.

- Only *Jeff is happy*, and nobody else is happy.

- *Everyone hugged themselves*, and nobody else hugged anybody else.

- Task: compute the interpretation of the following formulas:

(@) $\metalang{happy}(\metalang{Jeff})$

(@) $\metalang{hugs}(\metalang{Annie},\metalang{Annie})$

## Solution

$$
&\begin{aligned}[t]
&\evalM[g]{\metalang{happy}(\metalang{Jeff})}\\
&=\Set{⟨s,s'⟩|s' = \widehat{sj} ∧ j ∈ \entity{happy}}\\
&=\Set{⟨[],[j]⟩,⟨[x],[xj]⟩,⟨[xy],[xyj]⟩,...}
\end{aligned}
$$

$$
\begin{aligned}[t]
&\evalM[g]{\metalang{hugs}(\metalang{Annie},\metalang{Annie})}\\
&=\Set{⟨s,s'⟩|s' = \widehat{saa} ∧ ⟨a,a⟩ ∈ \entity{hugs}}\\
&= \Set{⟨[],[aa]⟩,⟨[x],[xaa]⟩,⟨[xy],[xyaa]⟩,...}
\end{aligned}
$$

## Variables

- Sentences with variables are just going to be tests on stacks.

$$
&\begin{aligned}[t]
&\evalM[g]{\metalang{happy}(x)}\\
&=\Set{⟨s,s'⟩|s' = s ∧ g(x) ∈ \entity{happy}}\\
&\metalang{if }g(x) = j\metalang{ then
}=\Set{⟨[],[]⟩,⟨[x],[x]⟩,⟨[xy],[xy]⟩,...}\\
&\metalang{else }∅
\end{aligned}
$$

## Falsity

- Just like in DPL, if an atomic formula is false in the model, it returns the
  empty set.
  
$$
&\begin{aligned}[t]
&\metalang{happy}(\metalang{Britta})\\
&=\Set{⟨s,s'⟩|s' = \widehat{sb} ∧ b ∈ \entity{happy}}\\
&=∅
\end{aligned}
$$

## Existential quantification

- Existentially quantified statement $∃v ϕ$ in this setting, are interpreted as
  instructions to update a stack $s$ with a random individual $x$, just so long
  as the embedded formula $ϕ$ is true relative to $g$, modified so that it maps
  $v$ to $x$.

- $\evalM[g]{∃ v ϕ} = \Set{⟨s,s'⟩|∃ x[s' = \widehat{sx} ∧ \evalM[g^{[v →
  x]}]{ϕ}]}$
  
- The idea is that existential quantifiers represent a *refusal to choose
  between different possible referents*
  
## Illustration

- Let's say we're in a model, again with $j$, $b$, and $a$, and only $j$ and $b$ arrived.
 
$$
\begin{aligned}[t]
&\evalM[g]{∃ v[\metalang{arrive}(v)]}\\
&= \Set{⟨s,s'⟩| ∃ x[s' = \widehat{sx} ∧ \evalM[g^{[x →
v]}]{\metalang{arrive}(v)}]}\\
&=\Set{⟨s,s'⟩| ∃ x[s' = \widehat{sx} ∧ x ∈ \entity{arrive}]}\\
&=\Set{\begin{aligned}[c]
&⟨[],[j]⟩\\
&⟨[],[b]⟩\\
&⟨[x],[xj]⟩\\
&⟨[x],[xb]⟩\\
&⟨[xy],[xyj]⟩\\
&⟨[xy],[xyb]⟩
\end{aligned}}
\end{aligned}
$$

## Pronouns

- How do pronouns pick up referents?

- Unlike DPL, we distinguish both syntactically and semantically between pronoun
  binding and variable binding.
  
- The rule for formulas with $\textit{pro}$ is going to be the following:

- If $π$ is a unary predicate, then $\evalM[g]{π(\textit{pro})} =
  \Set{⟨s,s'⟩| s' = s \metalang{ and } s'_τ ∈ \evalM{π}}$
  
- The idea is that pronouns return *the last object to be added to the stack*.

## Pronouns

- Of course this means we'll need to multiply our rules for binary connectives, so that we can deal with combinations of proper names and pronouns (and variables and pronouns, but I won't show that here).

- If $π$ is a binary predicate and $α$ is an individual constants, then $\evalM[g]{π(α,\textit{pro})} =
  \Set{⟨s,s'⟩| s' = \widehat{s\evalM[g]{α}} \metalang{ and } ⟨\evalM[g]{α},s'_τ⟩ ∈ \evalM[g]{π}}$
  
- If $π$ is a binary predicate and $α$ is an individual constant, then $\evalM[g]{π(\textit{pro},α)} =
    \Set{⟨s,s'⟩| s' = \widehat{s\evalM[g]{α}} \metalang{ and } ⟨s_τ,\evalM[g]{α}⟩
    \in \evalM[g]{\pi}}$
    
- Notice that our interpretation rules for binary predicates with $\textit{pro}$
  are *internally dynamic* -- when $\textit{pro}$ is the object, it picks up the
  referent pushed to the stack by the subject.
  
- Because of the way the rules are defined, the reverse doesn't go through.

## Pronouns ii

(@) Annie hugged herself -- $\metalang{hugged}(\metalang{Annie},\textit{pro})$

$$
= \Set{⟨s,s'⟩ | s' = \widehat{sa} ∧ ⟨a,s'_τ⟩ ∈ \entity{hug}}
$$

- This is guaranteed to be true in our model, since the pronoun picks up the
  discourse referent introduced by the subject.

(@) She hugged Annie. – $\metalang{hugged}(\textit{pro},\metalang{Annie})$

- The meaning here is dependent on the incoming stack $s$.

$$
= \Set{⟨s,s'⟩ | s' = \widehat{sa} ∧ ⟨a,s_τ⟩ ∈ \entity{hug}}
$$

## Pronouns iii

- Oh look! We've derived a basic version of Condition C of the binding theory.

(@) Annie$^1$ loves herself$₁$.

(@) \*she$_1$ loves Annie$^1$.

- Of course we need to refine the theory to get a broad empirical converage.

- In particular, we probably want to allow pronouns to pick up not just the last
  discourse referent added to the stack, to account for binding of pronouns
  across other NPs.
  
(@) Annie$^1$ thinks that Bill$^2$ hugged her$_1$.

## Dynamic binding

- Existential quantifiers can bind an object $\textit{pro}$ in exactly the same
  way.
  
$$
\begin{aligned}[t]
&\evalM[g]{∃ v[\metalang{hugged}(v,\textit{pro})]}\\
&= \Set{⟨s,s'⟩|∃ x[s' = \widehat{sx} ∧ \evalM[g^{[v → x]}]{\metalang{hugged}(v,\textit{pro})}]}\\
&= \Set{⟨s,s'⟩|∃ x[s' = \widehat{sx} ∧ ⟨x,s'_τ⟩ ∈ \entity{hugged}]}\\
&= \Set{\begin{aligned}[c]
&⟨[],[j]⟩\\
&⟨[],[b]⟩\\
&⟨[],[a]⟩\\
&⟨[x],[xj]⟩\\
&⟨[x],[xb]⟩\\
&⟨[x],[aa]⟩\\
...
\end{aligned}}
\end{aligned}
$$

## Dynamic binding and crossover

- But not a subject $\textit{pro}$!

$$
\begin{aligned}[t]
&\evalM[g]{∃ v[\metalang{hugged}(\textit{pro},v)]}\\
&= \Set{⟨s,s'⟩|∃ x[s' = \widehat{sx} ∧ \evalM[g^{[v → x]}]{\metalang{hugged}(\textit{pro},v)}]}\\
&= \Set{⟨s,s'⟩|∃ x[s' = \widehat{sx} ∧ ⟨s_τ,x⟩ ∈ \entity{hugged}]}\\
&= \Set{\begin{aligned}[c]
&⟨[j],[jj]⟩\\
&⟨[a],[aa]⟩\\
&⟨[b],[bb]⟩\\
&⟨[xj],[xjj]⟩\\
&⟨[xa],[xaa]⟩\\
&⟨[xb],[xbb]⟩\\
...
\end{aligned}}
\end{aligned}
$$

- The truth of this formula is dependent on the incoming stack already
  containing an individual that hugged themselves.
  
## Dynamic relations

- Without even saying anything about conjunction, we've already captured the
  fact that *binary predicates are internally dynamic*, something not captured
  by DPL.
  
- We did this, essentially, by lexical stipulation, but there are ways of
  reformulating \texttt{DyS} which make this more principled.
  
## Dynamic conjunction

- Conjunction in \texttt{DyS} is defined in the same way as DPL, but in terms of
  stacks rather than assignments:

- $\evalM[g]{[ϕ ∧ ψ]} = \Set{⟨s,s'⟩|∃ k:⟨s,k⟩ ∈ \evalM[g]{ϕ}\text{ and }⟨k,s'⟩ ∈ \evalM{ψ}}$

- We feed our input stack $s$ into the first conjunct, and return the result of
  feeding the output into the second conjunct.
  
## Cross-sentential binding

- Cross-sentential binding is captured in a totally straightforward way. Let's
  say that $j$ hugged $a$, everyone hugged themselves, nobody hugged anyone
  else, and only $j$ is happy.
  
(@) $∃ v[\metalang{hug}(v,\metalang{Annie})] ∧ \metalang{happy}(\textit{pro})$

$$
\begin{aligned}[t]
&\evalM[g]{∃ v[\metalang{\metalang{hug}(v,\metalang{Annie})}]}\\
&= \Set{⟨s,s'⟩|∃ x[s' = \widehat{sax} ∧ ⟨x,a⟩ ∈ \entity{hug}]}\\
&= \Set{⟨[],\alert{[aj]}⟩,⟨[],[aa]⟩,...}
\end{aligned}
$$

$$
\begin{aligned}[t]
&\evalM[g]{\metalang{happy}(\textit{pro})}\\
&= \Set{⟨s,s'⟩|s = s' ∧ s_τ ∈ \metalang{happy}}\\
&= \Set{⟨[j],[j]⟩,⟨\alert{[aj]},[aj]⟩,...}
\end{aligned}
$$

## Cross-sentential binding ii


$$
\begin{aligned}[t]
&\evalM[g]{∃ v[\metalang{hug}(v,\metalang{Annie})] ∧ \metalang{happy}(\textit{pro})}\\
&=\Set{⟨[],[aj]⟩,...}
\end{aligned}
$$

- Notice that the last discourse referent pushed to the stack is $j$.

- Binding isn't going to work in the other direction...

## Cross-sentential binding iii

$$
\begin{aligned}[t]
&\evalM[g]{\metalang{happy}(\textit{pro})}\\
&= \Set{⟨s,s'⟩|s = s' ∧ s_τ ∈ \metalang{happy}}\\
&= \Set{⟨[j],\alert{[j]}⟩,..}
\end{aligned}
$$

$$
\begin{aligned}[t]
&\evalM[g]{∃ v[\metalang{\metalang{hug}(v,\metalang{Annie})}]}\\
&= \Set{⟨s,s'⟩|∃ x[s' = \widehat{sax} ∧ ⟨x,a⟩ ∈ \entity{hug}]}\\
&= \Set{⟨[],[aj]⟩,⟨[],[aa]⟩,⟨\alert{[j]},[jaj]⟩,⟨\alert{[j]},[jaa]⟩,...}
\end{aligned}
$$

- It's obvious here that we're just going to get something with dynamic effects
equivalent to *someone hugged Annie*. 

## Backwards binding

- Note the predictions out theory makes:

(@) Someone$ˣ$ arrived. They$ₓ$ sat down.

(@) \*They$ₓ$ sat down and someone$ˣ$ arrived.

(@) Someone$ˣ$ likes themselves$ₓ$.

(@) \*They$ₓ$ like someone$ˣ$.

## Trace binding

 -  If traces are interpreted as variables, rather than as
    $\textit{pro}$, \texttt{DyS} predicts that traces cannot be
    dynamically bound.
    
 - It's difficult to come up with examples to test this but maybe something like
   the following. Although note that this would typically be rules out
   syntactically.
    
(@trace) Does John know who$ˣ$ left and $tₓ$ like Mary?

## Features of \texttt{DyS}

- Some interesting features of \texttt{DyS}:

    - We don't require co-indexation between pronouns and their binders, only
      between quantifiers and their traces.
      
    - Coreference and binding are not syntactically distinguished.
    
    - Stack updates are always monotonic – we can always add individuals to the
      stack, but never reduce it.
      
## Negation

- In \texttt{DyS}, additional operators can be added to achieve the same results
  as DPL.
  
- $\evalM[g]{\neg\phi} = \Set{⟨s,s'⟩| s = s' ∧ ¬∃ k:⟨s',k⟩ ∈ \evalM[g]{ϕ}}$

- $¬ϕ$ is interpreted as a test on stacks that *fail* to return a valid output
  when threaded into $ϕ$.
  
## Negation ii

- We can see how negation is going to block dynamic binding

- Since every stack is a possible input to the existential statement, negation
  is going to return the empty set.

 $$
\begin{aligned}[t]
&\evalM[g]{¬∃ v[\metalang{happy}(v)]}\\
&=\Set{⟨s,s'⟩| s = s' ∧ ¬k:⟨s',k⟩ ∈ \Set{⟨t,t'⟩|∃ x[t' = \widehat{tx} ∧ x ∈ \entity{happy}]}}\\
&=\Set{⟨s,s'⟩| s = s' ∧ ¬k:⟨s',k⟩ ∈ \Set{⟨[],[j]⟩,⟨[a],[aj]⟩,⟨[j],[jj]⟩,...}}\\
&=∅
 \end{aligned}
 $$
 
## Exercise

- Define the rule for dynamic implication in \texttt{DyS}. Here is the rule from
  DPL to help you:
  
  - $\evalM{ϕ → ψ} = \Set{⟨i,o⟩|o = i \metalang{ and }∀ k:⟨o,k⟩ ∈ \evalM{ϕ} ⇒ ∃
  j:⟨k,j⟩ ∈ \evalM{ψ}}$

- Once you've done that, compute the meaning of the following.

(@) If someone$ˣ$ hugs Annie, they$ₓ$ are happy.

- Again, assume a model where Jeff hugs Annie, everyone hugs themselves, nobody
  else hugs anybody else, and only Jeff is happy.

 
## The novelty condition

- Heim proposed the *novely condition* to rule out cases like the following:

(@) Someone$^1$ walked in. Someone$^1$ sat down.

- In frameworks like DPL, the second conjunct "resets" the value of $x_1$,
  predicting that the sentence as a whole should just introduce a discourse referent
  that sat down.
  
- \texttt{DyS} doesn't have any need for the novely condition, since each
  existential statement adds a new discourse referent to the stack. Indices are irrelevant.

## The novelty condition ii
  
$$
\begin{aligned}[t]
&\evalM[g]{∃ v[\metalang{walkedIn}(v)]}\\
&=\Set{⟨t,t'⟩|∃ x[t' = \widehat{tx} ∧ x ∈ \entity{walkedIn}]}
\end{aligned}
$$

$$
\begin{aligned}[t]
&\evalM[g]{∃ v[\metalang{satDown}(v)]}\\
&=\Set{⟨t,t'⟩|∃ y[t' = \widehat{ty} ∧ y ∈ \entity{satDown}]}\\
\end{aligned}
$$

$$
\begin{aligned}[t]
&\evalM[g]{∃ v[\metalang{walkedIn}(v)] ∧ ∃ v[\metalang{satDown}(v)]}\\
&=\Set{⟨t,t'⟩|∃ xy[t' = \widehat{txy} ∧ x ∈ \entity{walkedIn} ∧ y ∈ \entity{satDown}]}\\
\end{aligned}
$$

## The familiarity condition

- Heim also proposed the familiarity condition, to capture the fact that
  pronominals seem to presuppose the existence of a familiar discourse referent.
  
  (@) (In an out-of-the-blue context) \*He sat down.
  
- The way that Heim accomplishes this is by making assignments partial, and
  introducing a syntactic condition stating that
  pronouns should re-use an index that has already been introduced.
  
## The familiarity condition

- \texttt{DyS} can capture something like the familiarity condition.

- If $π$ is a unary predicate, then $\evalM[g]{π(\textit{pro})} =
  \Set{⟨s,s'⟩| s' = s \metalang{ and } s'_τ ∈ \evalM{π}}$
  
  - Recall that formulas with pronouns are tests on a stack $s'$, just in case
    $s'_τ$ satisfies the predicate.
    
  - $τ$ is a partial function that is only defined for stacks that have at least
    one discourse referent. If the input stack is empty, i.e., if there are no
    salient discourse referents, then $τ$ is undefined.
    
## Further directions

- Dynamic semantics was initially motivated by basic data concerning the
  interaction between scope and pronominal binding.

- But the empirical reach of dynamic semantics extends beyond this domain.

- Dynamic semantics is especially well-suited to analyzing phenomena which
  display a *dynamic signature* – an apparent sensitivity to linear order.

## Tense

 - Partee (1973) observed that their are parallels between tense and pronouns.
 
 - Consider the following data:
 
 (@) Pedro owns a donkey$^x$. He beats it$_x$.
 
 (@) Yesterday, Pedro tried$^t$ to kiss Juanita. She slapped$_{t ≤ t'}$ him.
 
 - The first sentence is a standard case of *donkey anaphora*. We are already
   equipped with the necessary tools to analyse this. 
   
- The second sentence shows a similar phenomenon, but it the temporal domain --
  the past tense interpretation of the second clause is anaphorically dependent
  on the first. The sentence is conveys that Juanita slapped Pedro *right after*
  he tried to kiss her.
  
## Tense ii

- The idea informally, is as follows. Action sentences are existentially
  quantified statements about events (Davidson, 1967). As well as more
  metaphysically conventional entities, we also have events in our domain.
  
(@) Pedro tried to kiss Juanita. -- $∃ e₁[\metalang{tryToKiss}(p,j,e₁) ∧ e₁ ≤ e]$
  
(@) She slapped him -- $∃ e₂[\metalang{slapped}(x,y,e₂) ∧ e₂ ≤ e₁]$
  
(@) Pedro tried to kiss Juanita and she slapped him.

$$
∃ \alert{e₁}[\metalang{tryToKiss}(p,j,e₁) ∧ e₁ ≤ e] ∧ ∃ e₂[\metalang{slapped}(x,y,e₂) ∧ e₂ ≤ \alert{e₁}]
$$

- If we treat the existential quantifier over events as our familiar *dynamic*
  existential quantifier, then binding across conjuncts is predicted to be possible.

## Donkey tense

- As well as cross-sentential tense anaphora, we can also create examples which
  exhibit the correlate of donkey anaphora in the temporal domain.
  
(@) Every farmer$^x$ who owns a donkey$^y$ $t_x$ beat it$_y$.

(@) Whenever Pedro tried$^t$ to kiss Juanita, she slapped$_{t ≤ t'}$ him.

$$
(∃ e₁[\metalang{tryToKiss}(p,j,e₁) ∧ e₁ ≤ e]) → (∃e₂[\metalang{slap}(x,y,e₂) ∧
e₂ ≤ e₁])
$$

# Presupposition

## The phenomenon

 - In this section we'll explore the other primary empirical motivation for a
   dynamic perspective on meaning.
    
 - Certain expressions in natural language seem to carry preconditions.
 
 (@) Mary stopped smoking
 
 - Sounds fine in contexts where it's known that Mary used to smoke...
   - and is *true* iff she doesn't currently smoke. 
 
 - Sounds weird in contexts where it's known that Mary didn't used to smoke...
   - ...and that she doesn't currently smoke.
   - ...and that she does currently smoke. 
 
## Assertion vs. presupposition

- Generally speaking, certain predicates seem to have *two dimensions* to their
  meaning:
  
- The *assertive component*, which determines their truth/falsity.
  - for *stopped smoking*, the assertive component is *doesn't currently smoke*.
  
- The *presupposition*, which determines *whether* the sentence has a chance of
  being true or false in the first place.
  - for *stopped smoking*, the presupposition is *used to smoke*.
  
  - I'll write the presupposition of an expression $P$ as $Π(P)$, and the assertion
    $Α(P)$.
 
## Presupposition projection

- One way of distinguish the assertion vs. the presupposition, is that certain
  operators, such as *negation* are *presupposition holes* – in other words,
  negation affects the assertion of the sentence (it is negated), but the
  presupposition is not.
  
(@) Jeff didn't stop smoking

- $Α$: it's not the case that Jeff currently smokes.
- $Π$: Jeff used to smoke.

- Negation *projects* through negation.
 
## Presupposition in a static setting

- In a static setting, the standard way of dealing with presupposition is to
treat predicate meanings as *partial functions*.

- $\metalang{stoppedSmoking} = λ x:\metalang{usedToSmoke}(x) . ¬\metalang{smokesNow}(x)$

- This is a *function* from an individual $x$, which is defined if $x$ used to
  smoke, and returns *true* iff $x$ doesn't currently smoke.
  
## Presupposition in a static setting

- The partial function $\metalang{stoppedSmoking}$ is applied to an individual,
  such as $\metalang{Jeff}$. Since $\metalang{stoppedSmoking}$ is *partial*, it
  will return a truth value iff *Jeff used to smoke* is true, if not the
  sentence will be undefined.
  
 $$
\begin{aligned}[t]
&[λ x:\metalang{usedToSmoke}(x) . ¬\metalang{smokesNow}(x)](\metalang{Jeff})\\
&\metalang{defined iff usedToSmoke}(\metalang{Jeff}) = 1\\
&= 1\metalang{ iff }¬\metalang{smokesNow}(\metalang{Jeff})
\end{aligned}
 $$
 
## Presupposition in a static setting
 
 - We can leave negation as a total function from truth values to true values.
 
 - $\metalang{not} = λ t . ¬t$
 
 - Functions are only defined, however, if their arguments are defined. If the
   argument $ϕ$ of $\metalang{not}$ is undefined, $\metalang{not}(ϕ)$ is
   undefined.
   
 - This basic story predicts projection through negation.
 
## Presupposition in a static setting
  
$$
\begin{aligned}[t]
&\metalang{not}([λ x:\metalang{usedToSmoke}(x) . ¬\metalang{smokesNow}(x)](\metalang{Jeff}))\\
&\metalang{defined iff usedToSmoke}(\metalang{Jeff}) = 1\\
&= 1\metalang{ iff }\metalang{smokesNow}(\metalang{Jeff})
\end{aligned}
 $$
 
## Trivalence

- Analyzing presuppositions using partial functions is largely *equivalent* to
  introducing a third truth value $\#$ to represent *undefined*.
  
- We can think of $\metalang{stoppedSmoking}$ as the following function.

$$
\metalang{stoppedSmoking} = λ x . \begin{cases}
\metalang{if }¬\metalang{usedToSmoke}(x)\metalang{ then }\#\\
\metalang{if }\metalang{usedToSmoke}(x) ∧ ¬\metalang{smokes}(x)\metalang{ then
}1\\
\metalang{if }\metalang{usedToSmoke}(x) ∧ \metalang{smokes}(x)\metalang{ then }0
\end{cases}
$$

## Capturing projection

- The final step is to assign logical operators such as not *trivalent
  meanings*.
  
  $$
  \metalang{not} = λ t . \begin{cases}
  t = \# &\#\\
  t = 0 &1\\
  t = 1 &0
  \end{cases}
  $$
  
## Back to FOL

- We can recast the static account of presupposition projection in our FOL
  framework by formulating trivalent rules for atomic and complex formulae.
  
- If $π$ is a unary predicate and $α$ is a individual constant, then:

- $\evalM[M,g]{π(α)} = \begin{cases}
      \metalang{ if }Π(π) = 0\metalang{ then }\#\\
      \metalang{ if }Π(π) = 1\metalang{ and } \evalM[M,g]{α} ∈
      \evalM[M,g]{π}\metalang{ then }1\\
      \metalang{ if }Π(π) = 1\metalang{ and } \evalM[M,g]{α} ∉
      \evalM[M,g]{π}\metalang{ then }0
    \end{cases}$

- $\evalM[M,g]{¬ϕ} = \begin{cases}
\evalM[M,g]{ϕ} = \# &\#\\
\evalM[M,g]{ϕ} = 0 &1\\
\evalM[M,g]{ϕ} = 1 &0\\
\end{cases}$

## Exercise

- Formulate trivalent interpretation rules for the rest of the logical
  connectives. Come up with examples to determine what happens with $\#$.
  
- $\evalM[M,g]{ϕ ∧ ψ} = 1 \metalang{iff} \evalM[M,g]{ϕ} = 1 \metalang{and} \evalM[M,g]{ψ}
  = 1$
- $\evalM[M,g]{ϕ ∨ ψ} = 1 \metalang{iff} \evalM[M,g]{ϕ} = 1 \metalang{or} \evalM[M,g]{ψ}
  = 1$
- $\evalM[M,g]{ϕ → ψ} = 1 \metalang{unless} \evalM[M,g]{ϕ} = 1 \metalang{and} \evalM[M,g]{ψ}
  = 0$
- $\evalM[M,g]{ϕ ↔ ψ} = 1 \metalang{iff} \evalM[M,g]{ϕ} = \evalM[M,g]{ψ}$

## A problem for static theories of presupposition

- Consider the following sentence:

(@) Jeff used to smoke and he stopped smoking.

- Do you think this *complex* sentence is presuppositional?

## Using negation as a diagnostic

(@) It's not the case that Jeff used to smoke and he stopped smoking.

 - This is true iff Jeff used to smoke, and doesn't currently smoke!
 
 - We can conclude that this complex sentence *doesn't have a presupposition*
 
## Trivalence?

- Let's check that our trivalent formulation of *and* isn't going to derive
  this.
  
 $\evalM[M,g]{ϕ ∧ ψ} = \begin{cases}
 \evalM[M,g]{ϕ} = \#\metalang{ or } \evalM[M,g]{ψ}
  = \# &\#\\
 \metalang{else }\evalM[M,g]{ϕ} = 0\metalang{ or } \evalM[M,g]{ψ}
  = 0 &0\\
 \metalang{otherwise }&1
 \end{cases}$
 
 - We predict, in fact that *Jeff used to smoke, and stopped smoking* should
   *inhert* the presupposition of *stopped smoking*.
   
- In other words, the presupposition of *stopped smoking* should project, but it
  doesn't
  
## Back to Dynamic Semantics

(@) Jeff used to smoke, and he stopped smoking.

- Intuitively, the reason why this sentence lacks a presupposition, because the
  first conjunct asserts what is presupposed by the second sentence.
  
- Note that linear order seems to matter here.

(@) Jeff stopped smoking and he used to smoke.

- We find similar affects with implication.

(@) If John used to smoke, then he stopped smoking.

## The Stalnakerian model of discourse

- Stalnaker's idea is that when we *assert* a sentence $ϕ$, the common ground
  $C$ is *updated* with the content of $ϕ$.
  
(@) Jeff smokes $= C[\metalang{Jeff smokes}]$

- So far, we have been assuming that sentences are simply *true* or *false* (or
  maybe undefined). If sentences can only contribute truth-values, it's
  difficult to make sense of the idea that they can add information to the
  common ground.
  
- In order to make sense of this idea, we need to shift to a richer notion of
  content, so that we can distinguish between the informational content of
  sentences, regardless of whether they are true or false.
  
## Possible worlds

- We will posit a new semantic object – a *possible world*.

- A possible world is simply a *way the world could be*, i.e., an index at which
  all of the facts are known.
  
- For example, there may be a world $w₁$, in which it's true that Jeff smokes,
  and a world $w₂$ where it's false that Jeff smokes.
  
- Philosophers argue frequently about the ontological nature of these beasts.
  For our purposes, they're a convenient mathematical construct for capturing
  the idea that we can reason about possibilties.
  
## Possible worlds and propositions

- Possible worlds are indices at which *all of the facts are known*.

- So, at $w₁$, Jeff will have properties other than just smoking, maybe he's
  e.g., blonde. We will need to multiply our possible worlds to account for the
  range of possibilities:
  
  - $w₁ =$Jeff smokes and is blonde
  - $w₂ =$Jeff doesn't smoke and is blonde
  - $w₃ =$Jeff smokes and is brunette
  - $w₄ =$ Jeff doesn't smoke and is brunette
  
## Possible worlds and propositions ii

- Rather than thinking of a sentence such as *Jeff smokes* as expressing a
  *truth value*, we can think of it as expressing a *set of possible worlds*,
  namely, those possible worlds at which the sentence is true.
  
- Jeff smokes $=\Set{w₁,w₃}$
- Jeff doesn't smoke $=\Set{w₂,w₄}$
- Jeff is blonde $=\Set{w₁,w₂}$
- Jeff is brunette $=\Set{w₃,w₄}$

## Enriching FOL

 - Our *model* will now not just include a *domain* $D$, an *interpretation
   function* $I$, and a stock of *variables* $V$, but also a set of *possible
   worlds* $W$.
   
$$
M = ⟨D, I, V, W⟩
$$

- Our interpretation function $I$ now takes an additional *possible world*
  argument, to reflect that fact that predicates can be true of different
  individuals in different worlds. We'll right $I_w(α)$ to express the
  interpretation of $α$ at $w$.
  
## From Extensions to Intensions

- We can now reformulate our rule for atomic formulas, such that they express
  *sets of possible worlds* (intensions), rather than truth values (extensions).
  
- If $π$ is a unary predicate and $α$ is a individual constant, then:
    - $\evalM[M,g,w']{ϕ(α)} = \Set{w | w ∈ W ∧ \evalM[M,g,w]{α} ∈ \evalM[M,g,w]{π}}$

- This move is independently motivated, on the basis of verbs such as *believe*, etc.

## Back to Stalnaker

- We can now make sense of Stalnaker's idea in a more precise way – the common
  ground can be thought of as just a *set of worlds*.
  
- For example, if we're not sure whether Jeff smokes or whether he's blonde or
  brunette, then the common ground $C=\Set{w₁,w₂,w₃,w₄}$.
  
- Our rule for assertion states that, on asserting $ϕ$, we *intersect* $ϕ$ with
  $C$.
  
- Jeff smokes $=C ∩ \Set{w | w ∈ W ∧ j ∈ I_w(\metalang{smoke})}$

- $=\Set{w₁,w₂,w₃,w₄} ∩ \Set{w₁,w₃} = \Set{w₁,w₃}$

## Back to Stalnaker ii

- Contradictory sentences – those which are true in no possible world – simply
  express the empty set $∅$.
  
- Asserting a contradiction will always result in $∅$, since intersecting $∅$
  with any other set always returns $∅$.
  
## Back to Stalnaker iii

- On to Stalnaker's innovation – a conjunctive sentence such as *Jeff smokes and
  Jeff is blonde* involves *incrementally updating* the common ground $C$, in
  other words:
  
- Jeff smokes and Jeff is blonde = $\metalang{C[Jeff smokes][Jeff is blonde]}$

- $C ∩ \metalang{Jeff smokes} = \Set{w₁,w₃}$

- $\Set{w₁,w₃} ∩ \metalang{Jeff is blonde} = \Set{w₁,w₃} ∩ \Set{w₁,w₂} = \Set{w₁}$

- The effect of incrementally updating $C$ with first *Jeff smokes* followed by
  *Jeff is blonde* involves narrowing down $C$ until we are just left with the
  world in which Jeff smokes and is blonde.
  
## The flow of information

- In this way Stalnaker's framework involves modelling the flow of information
  over the course of a discourse.
  
- The more information we learn, the more we can narrow down $C$, which reflects
  how certain we are about how the world is.
  
- To see the similarities with the dynamic systems we've been looking at,
  suppose that we think of the meaning of an atomic formula as a *relation
  between sets of possible worlds*, i.e., an instruction for updating $C$.
  
$$
\metalang{Jeff smokes} = \Set{⟨ϕ,ψ⟩|ψ = ϕ ∩ \Set{w| w ∈ W ∧ j ∈ I_w(\metalang{smokes})}}
$$

## Stalnakerian conjunction

- Just like in dynamic semantics, we can think of conjunction as an iterative
  update.
  
$$
\begin{aligned}[t]
&p ∧ q\\
&= \Set{⟨ϕ,ψ⟩|ψ = (ϕ ∩ p) ∩ q}
\end{aligned}
$$

## Stalnaker's proposal

- Let's consider our problematic example again.

(@) Jeff used to smoke and Jeff stopped smoking.

- Using our dynamic interpretation scheme, this will be interpreted as:

$$
\Set{⟨ϕ,ψ⟩|ψ = \begin{aligned}[c]
    &(ϕ ∩ \set{w|j ∈ I_w(\metalang{smoked})})\\
    &∩ \Set{w' | j ∈ I_{w'}(\metalang{stoppedSmoking})}
    \end{aligned}}
$$

$$
Π(\metalang{Jeff stopped smoking}) = \metalang{John smoked}
$$

## Stalnaker's proposal ii

- Since $Π(\metalang{stoppedSmoking})$ is entailed by $Α(\metalang{smoked})$,
this incremental update will *always* be defined.

- Therefore, our interpretation is equivalent to:

$$
\Set{⟨ϕ,ψ⟩|ψ = \begin{aligned}[c]
    &(ϕ ∩ \set{w|j ∈ I_w(\metalang{smoked})})\\
    &∩ \Set{w' | ¬(j ∈ I_{w'}(\metalang{smoke}))}
    \end{aligned}}
$$

- which is presuppositionless! The presupposition is *locally satisfied* by the
  first conjunct.
  
## Function composition

- Let's try to reformulate this idea in terms of functions to make sure we
  understand the basic idea.

$$
P ∘ Q = λ x . P (Q x)
$$
  
## Stalnaker's proposal (with functions)

$$
P = λ p . [λ w . j ∈ \entity{smoked}] ∧ p 
$$

$$
Q = λ p . [λ w:j ∈ \entity{smoked} . ¬(j ∈ \entity{smokes})] ∧ p
$$

$$
\begin{aligned}
&P\metalang{ and }Q = λ p . Q ∘ P ∘ p\\
&= λ p .  [λ w . j ∈ \entity{smoked} ∧ ¬(j ∈ \entity{smokes})] ∧ p 
\end{aligned}
$$

$$
Q_{Π} ∘ P ≡  Q ∘ P\metalang{ iff }P ⇒ Π(Q)
$$

## Other operators

- To a large extent, structural conditions on local satisfaction of
  presuppositions are identical to those of dynamic binding.
  
(@) If John used to smoke$^{Π}$ then he stopped smoking$_{Π}$

(@) If a man$^x$ arrived, then he$ₓ$ sat down.

(@) Every man who used to smoke$^{Π}$ has stopped smoking$_{Π}$

(@) Every man who met a professor$^x$ admired her$ₓ$.

## Accessibility

- The same operators that block dynamic binding block local satisfaction of a
  presupposition.
  
(@) \*John didn't ever smoke$^{Π}$ and he stopped smoking$_{Π}$.

(@) \*Nobody$ˣ$ left and he$ₓ$ sat down.

(@) \*If Mary used to smoke$^{Π}$ then John did too. She stopped smoking$_{Π}$.

(@) \*If someoned$ˣ$ arrived, they$ₓ$ sat down. They$ₓ$ left soon afterwards. 

## The underlying dynamics

- Just as we analyze the dynamic nature of anaphora by treating sentence
  meanings as relations between assignments (or stacks if you prefer), we can
  analyse the dynamic nature of presupposition satisfaction by treating sentence
  meanings as relations between propositions.
  
- Ultimately, we want to have a single logical system which provides a
  unification of both ideas.
  
- For such a system, see for example @rothschild2017.

- I won't try to lay out the formal details here.

## A problem

- A brief note: a dynamic theory of presupposition satisfaction inherits much of
  the same problems as a dynamic approach to anaphora.
  
(@) Either nobody$^x$ is coming, or they$ₓ$ are late.

(@) Either Jane hasn't ever smoked$^{Π}$, or she stopped smoking$_{Π}$.

- The local antecedent to both the anaphor and the presupposition should be
  rendered inaccessible – but it isn't.
  
## Overview

- In this course, I've taken you from the most bare-bones logical system
  necessary for analysing a fragment of natural language – namely L~1~, to a
  dynamic logical system \texttt{DyS}, which accounts for the behaviour of
  anaphora and binding in a wide variety of environments.
  
- To do this, we've incrementally built on L~1~ adding new features as we
  encountered new data that motivated them.
  
- Throughout, we've maintained the core tenets of formal semantics – namely,
  that we can analyze meaning in natural language using a rigorous procedure for
  mapping expressions in a logical language to *meanings*.
  
  - In L~1~, meanings were *truth values*.
  - In FOL, meanings were *sets of assignments*.
  - In DPL and \texttt{DyS}, meanings were *relations between assignments (or stacks)*
  - In a Stalnakerian setting, meanings were *relations between propositions*

## Overview ii

- The methods and formal tools available allow us to build on the results of our
  earlier theories *without throwing the baby out with the bathwater*.
  
- In the final class, we've given an overview of phenomena that motivate the
  dynamic perspective beyond just anaphora and binding.
  
- Even if you don't plan on being a formal semanticists, hopefully we've managed
  to convey the advantages to analyzing natural language with the level of
  rigour and precision that formal tools afford us.
  
# Thank you!
